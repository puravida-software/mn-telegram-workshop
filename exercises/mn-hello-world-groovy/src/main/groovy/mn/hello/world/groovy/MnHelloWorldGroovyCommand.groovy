package mn.hello.world.groovy

import javax.inject.Inject;

import com.puravida.mn.telegram.Message;
import com.puravida.mn.telegram.TelegramBot;

import io.micronaut.configuration.picocli.PicocliRunner
import io.micronaut.context.ApplicationContext

import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import picocli.CommandLine.Parameters

@Command(name = 'mn-hello-world-groovy', description = '...',
        mixinStandardHelpOptions = true)
class MnHelloWorldGroovyCommand implements Runnable {

    @Option(names = ['-v', '--verbose'], description = '...')
    boolean verbose

    @Option(names = ["-c", "--channel"], description = "channel", required = true)
    String channelId

    static void main(String[] args) throws Exception {
        PicocliRunner.run(MnHelloWorldGroovyCommand.class, args)
    }

    @Inject
    TelegramBot telegramBot;

    void run() {
        // business logic here
        if (verbose) {
            println "Hi!"
	    return
        }
	telegramBot.sendMessage(new Message(chat_id:channelId,text:"hello world")).blockingGet()
    }
}
