package mn.hello.world;

import javax.inject.Inject;

import com.puravida.mn.telegram.Message;
import com.puravida.mn.telegram.TelegramBot;

import io.micronaut.configuration.picocli.PicocliRunner;
import io.micronaut.context.ApplicationContext;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "mn-hello-world", description = "...",
        mixinStandardHelpOptions = true)
public class MnHelloWorldCommand implements Runnable {

    @Option(names = {"-v", "--verbose"}, description = "...")
    boolean verbose;

    @Option(names = {"-c", "--channel"}, description = "channel", required = true)
    String channelId;

    public static void main(String[] args) throws Exception {
        PicocliRunner.run(MnHelloWorldCommand.class, args);
    }

    @Inject
    TelegramBot telegramBot;

    public void run() {
        // business logic here
        if (verbose) {
            System.out.println("Hi!");
            return;
        }
        Message message = new Message();
        message.setChat_id(channelId);
        message.setText("hello world");
        telegramBot.sendMessage(message).blockingGet();
    }
}
