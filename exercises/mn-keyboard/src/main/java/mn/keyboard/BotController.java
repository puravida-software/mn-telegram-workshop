package mn.telegram.chat;

import java.util.List;
import java.util.ArrayList;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.reactivex.Single;
import io.micronaut.core.io.ResourceResolver;
import com.puravida.mn.telegram.*;

@Controller("${telegram.token}")
public class BotController{

    TelegramBot telegramBot;

    BotController(TelegramBot telegramBot){
        this.telegramBot = telegramBot;
    }

    @Post
    String index( Update update){
        
        if( update.getMessage() != null ){
            sendKeyboard(update);
        }else{
            answerKeyboard(update);
        }

        return "done";
    }

    void sendKeyboard(Update update){
        Single.create( emitter -> {
                        
            InlineKeyboardButton button;
            List<InlineKeyboardButton> row;
            List<List<InlineKeyboardButton>> keyboard = new ArrayList();

            row = new ArrayList();

            button = new InlineKeyboardButton();
            button.setText("Hi");
            button.setCallback_data("hi");
            row.add(button);

            button = new InlineKeyboardButton();
            button.setText("Hola");
            button.setCallback_data("hola");
            row.add(button);

            keyboard.add(row);

            button = new InlineKeyboardButton();
            button.setText("Cancel");
            button.setCallback_data("cancel");
            row = new ArrayList();
            row.add(button);
            keyboard.add(row);

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            inlineKeyboardMarkup.setInline_keyboard(keyboard);

            Message message = new Message();
            message.setChat_id( update.getMessage().getChat().getId() );
            message.setText("A keyboard");
            message.setReply_markup(inlineKeyboardMarkup);

            telegramBot.sendMessage(message).blockingGet();

            emitter.onSuccess("done");            
        }).subscribe();
    }

    void answerKeyboard(Update update){
        String chatId = update.getCallback_query().getMessage().getChat().getId();
        int messageId = update.getCallback_query().getMessage().getMessage_id();
        switch(update.getCallback_query().getData()){
            case "hi":{
                Message message = new Message();
                message.setChat_id( chatId );
                message.setText("goodbye");
                telegramBot.sendMessage(message).blockingGet();
            }
            break;
            case "hola":{
                Message message = new Message();
                message.setChat_id( chatId );
                message.setText("adios");
                telegramBot.sendMessage(message).blockingGet();
            }
            break;
            case "cancel":                                
                telegramBot.deleteMessage( chatId, messageId).blockingGet();
            break;
        }
    }

}