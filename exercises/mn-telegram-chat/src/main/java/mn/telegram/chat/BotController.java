package mn.telegram.chat;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.reactivex.Single;
import io.micronaut.core.io.ResourceResolver;
import com.puravida.mn.telegram.*;

@Controller("${telegram.token}")
public class BotController{

    TelegramBot telegramBot;

    ResourceResolver resourceLoader;

    BotController(TelegramBot telegramBot, ResourceResolver resourceLoader){
        this.telegramBot = telegramBot;
        this.resourceLoader = resourceLoader;
    }

    @Post
    String index( Update update){
        
        Single.create( emitter -> {
            
            Message message = new Message();
            message.setChat_id(update.getMessage().getChat().getId());
            message.setText("Hi");

            telegramBot.sendMessage(message).blockingGet();


            byte[]bytes = resourceLoader.getResource("classpath:tomatoma.mp4").get().openStream().readAllBytes();
            SendAnimation sendAnimation = new SendAnimation();
            sendAnimation.setChat_id(update.getMessage().getChat().getId());
            sendAnimation.setBytes(bytes);
            telegramBot.sendAnimation(sendAnimation.multipartBody()).blockingGet();
            
            emitter.onSuccess("done");            
        }).subscribe();

        return "done";
    }

}