package mn.nasa;

import javax.inject.Inject;

import com.puravida.mn.telegram.*;

import io.micronaut.configuration.picocli.PicocliRunner;
import io.micronaut.context.ApplicationContext;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import com.jayway.jsonpath.*;

@Command(name = "mn-nasa", description = "...",
        mixinStandardHelpOptions = true)
public class MnNasaCommand implements Runnable {

    @Option(names = {"-v", "--verbose"}, description = "...")
    boolean verbose;

    @Option(names = {"-c", "--channel"}, description = "channel", required = true)
    String channelId;

    public static void main(String[] args) throws Exception {
        PicocliRunner.run(MnNasaCommand.class, args);
    }

    @Inject
    TelegramBot telegramBot;

    public void run(){
        // business logic here
        if (verbose) {
            System.out.println("Hi!");
            return;
        }
        try{
            String nasaURL = "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY";
            String nasaJSON = org.apache.commons.io.IOUtils.toString(new java.net.URL(nasaURL));
            Object document = Configuration.defaultConfiguration().jsonProvider().parse(nasaJSON);

            String title = JsonPath.read(document, "$.title");
            String explanation = JsonPath.read(document, "$.explanation");
            String imageURL  = JsonPath.read(document, "$.url");

            Message message = new Message();
            message.setChat_id(channelId);
            message.setText("*"+title+"*\n"+explanation);
            telegramBot.sendMessage(message).blockingGet();

            ReadableByteChannel readableBC = Channels.newChannel( new java.net.URL(imageURL).openStream() );
            File image = File.createTempFile("nasa", ".png");
            FileOutputStream fos = new FileOutputStream( image );
            fos.getChannel().transferFrom(readableBC, 0, Long.MAX_VALUE);

            System.out.println(image.getAbsolutePath());

            SendPhoto sendPhoto = new SendPhoto();
            sendPhoto.setChat_id(channelId);
            sendPhoto.setFile(image);
            sendPhoto.setCaption("NASA");
            telegramBot.sendPhoto(sendPhoto.multipartBody()).blockingGet();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
