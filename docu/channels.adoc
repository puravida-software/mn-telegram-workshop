= Channels

== Public

- Everyone can read it
- @TheChannel

== Private

- need the id
- -100xxxxxxxx

== Exercice 1

Create a private Channel

Create a micronaut cli to send a "hello world" to it

[source]
----
$ mn 
mn> create-cli-app mn-hello-world
----

.build.gradle
[source]
----
implementation group: 'com.puravida.telegram', name: 'mn-telegram-api', version: '0.1.9'
----

.MnHelloWorldCommand.java
[source]
----
Add an @Option channelId argument

@Inject TelegramBot

Construct a new Message and send it 

Build and execute (don't forget your telegram.token variable)
----

(see exercises/mn-hello-world project)

== Exercice 2

Send the Picture of the Day from NASA to the channel

[source]
----
$ mn 
mn> create-cli-app mn-nasa
----

.build.gradle
[source]
----
implementation "io.micronaut:micronaut-http-client"
implementation group: 'com.puravida.telegram', name: 'mn-telegram-api', version: '0.1.9'
implementation group: 'com.jayway.jsonpath', name: 'json-path', version: '2.4.0'
implementation group: 'commons-io', name: 'commons-io', version: '2.6'
----


.MnNasaCommand.java
[source]
----
Add an @Option channelId argument

@Inject TelegramBot

Parse the JSON 

    String nasaURL = "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY";
    String nasaJSON = org.apache.commons.io.IOUtils.toString(new java.net.URL(nasaURL));
    Object document = Configuration.defaultConfiguration().jsonProvider().parse(nasaJSON);

    String title = JsonPath.read(document, "$.title");
    String explanation = JsonPath.read(document, "$.explanation");
    String imageURL  = JsonPath.read(document, "$.url");


Construct a new Message and send the title and the explanation
Construct a SendPhoto to send the bytes of the image

Build and execute (don't forget your telegram.token variable)
----


(see exercises/mn-nasa project)


== Next

link:advanced.html[Advanced]