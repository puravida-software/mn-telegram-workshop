= Chat

Let's talk with our bot

== Create the app

[source,console]
----
$ mn create-app mn-telegram-chat
----

.build.gradle
[source,groovy]
----
    implementation group: 'com.puravida.telegram', name: 'mn-telegram-api', version: '0.3.2'
----

.application.yml
[source,yaml]
----
micronaut:
  application:
    name: mn-telegram-chat

telegram:
  token: CHANGEME
----

.BotController.java
[source,java]
----
package mn.telegram.chat;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.reactivex.Single;

import com.puravida.mn.telegram.*;

@Controller("${telegram.token}")
public class BotController{

    TelegramBot telegramBot;
    BotController(TelegramBot telegramBot){
        this.telegramBot = telegramBot;
    }

    @Post
    String index( Update update){
        
        Single.create( emitter -> {
            
            Message message = new Message();
            message.setChat_id(update.getMessage().getChat().getId());
            message.setText("Hi");

            telegramBot.sendMessage(message).blockingGet();
            
            emitter.onSuccess("done");            
        }).subscribe();

        return "done";
    }

}
----

Run your application:

'$ ./gradlew run'

Test with http (or curl):

'http POST localhost:8080/CHANGEME'

== Go live

Update `application.yml` with your token

Rebuild and run your application again with `./gradlew run`

Create a tunnel with internet `$ $HOME/ngrok http 8080`

Pay attention to you "public" url , i.e. http://38842c8e.ngrok.io

Tell to Telegram where to locate your bot 

`curl https://api.telegram.org/bot12345:AAAAAAAAAAAAAAAAAA/setWebhook?url=https://38842c8e.ngrok.io/12345:AAAAAAAAAAAAAAAAAA`

Say 'hello' to your bot using your mobile phone (or the web app)

== Send media

=== Send a git

Use `SendAnimation`

=== Send an image

Use `SendPhoto`

=== Send an audio

Use `SendAudio`

=== Howto

[source,java]
----
SendAnimation sendAnimation = new SendAnimation();
sendAnimation.setChat_id(update.getMessage().getChat().getId());
sendAnimation.setBytes(bytes); // Or also sendFile(file)
telegramBot.sendAnimation(sendAnimation.multipartBody()).blockingGet();
----


== Next

link:keyboard.html[Keyboards]